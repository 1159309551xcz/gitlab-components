
## mostlybroken components for gitlab ci

Gitlab components for use with the MostlyBroken CI-Stack

Requires a runner that pickup the tags `MostlyBroken` and `amd64`

### simple-container

simple container project with semantic tags `v0.0.0`

build by default linux amd64/arm64 containers

tagging scheme:

    - main-head:             :next
    - develop-head:          :testing
    - tag `v0.0.0`:          :latest
                             :0.0.0
                             :0.0
                             :0
    - tag `v0.0.0-appendix`: :0.0.0-appendix
